import datetime
import json
import os
import random
from pprint import pprint
from time import sleep

import requests
from faker import Faker
from geopy.geocoders import Nominatim

# ---------------------------------------
# Variables de entorno
# ---------------------------------------

API_IP = (os.environ["API_IP"])

# ---------------------------------------
# Generación de datos
# ---------------------------------------
def generarPersona():
    """
    Generates a random name and a random but valid nif of a spanish person

    Returns
    -------
    A name and a nif
    """

    f = Faker(locale="es_ES")

    return f.name(), f.nif()


def generarUbicacion():
    """
    Generates a location based on a place chosen depending on the incident given

    Returns
    -------
    Geografic coordenates and a place (str)
    """

    encontrado = False

    while not encontrado:
        try:
            latitud = random.uniform(36, 43.7911111111111)
            longitud = random.uniform(-3.32, -9.30222222222222)

            provincia = getProvincia(latitud, longitud)
            if provincia != "Otra ubicacion":
                encontrado = True
        except:
            sleep(5)

    return latitud, longitud, provincia


def generarIncidente():
    """
    Generates an incident

    Returns
    -------
    Incident chosen (str)
    """

    # Definimos los distintos incidentes posibles
    incidentes = ["Incendio", "Terremoto", "Inundación", "Explosión", "Atentado", "Brote viríco", "Apocalipsis Zombie",
                  "Tiroteo", "Tornado", "Agresión", "Accidente", "Erupción volcánica", "Ola de calor", "Diluvio",
                  "Sequía", "Tormenta", "Apagón", "Otro"]

    # Escogemos un incidente al azar
    return random.choice(incidentes)


def generarTitulo(incidente):
    """
    Generates an alert title based on the incident and the place

    Parameters
    ----------
    incidente : str
        Name of the incident reported

    Returns
    -------
    Title of the alert (str)
    """
    # Definimos distintas listas para poder generar distintos títulos
    numAfectados = range(1, 100)  # Número de posibles personas afectadas
    afectados = ["personas", "animales", "casas", "edificios", "coches"]  # Posibles afectados generales
    adjetivos = ["fuerte", "débil", "grande", "pequeño", "diminuto", "nuevo", "regional", "civil", "doble", "triple"]

    if incidente != "Otro":
        verbos = {"Incendio": ["Se ha generado", "Se ha creado", "Ha aparecido", "Se ha presenciado", "Se ha prendido",
                               "Se ha visto", "Ha empezado", "Ha comenzado", "Hay rumores de"],
                  "Terremoto": ["Se ha visto", "Se ha sentido", "Se ha presenciado", "Hay rumores de"],
                  "Inundación": ["Se ha visto", "Se ha sentido", "Se ha presenciado", "Ha aparecido", "Ha comenzado",
                                 "Ha empezado",
                                 "Hay rumores de"],
                  "Atentado": ["Se ha visto", "Se ha sentido", "Se ha presenciado", "Se ha oído", "Se teme",
                               "Se ha provocado",
                               "Se produjo", "Hay rumores de"],
                  "Explosión": ["Se ha visto", "Se ha sentido", "Se ha presenciado", "Se ha oído", "Se teme",
                                "Se ha provocado",
                                "Se produjo", "Hay rumores de"],
                  "Brote viríco": ["Se ha generado", "Se teme", "Se produjo", "Hay", "Hay rumores de"],
                  "Apocalipsis Zombie": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se ha provocado",
                                         "Se produjo",
                                         "Ha empezado", "Ha comenzado", "Hay rumores de"],
                  "Tiroteo": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se ha provocado",
                              "Se produjo",
                              "Ha empezado", "Ha comenzado", "Hay rumores de"],
                  "Tornado": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se ha provocado",
                              "Se produjo",
                              "Ha empezado", "Ha comenzado", "Hay rumores de"],
                  "Agresión": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se ha provocado",
                               "Se produjo",
                               "Ha empezado", "Ha comenzado", "Hay rumores de"],
                  "Accidente": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se ha provocado",
                                "Se produjo",
                                "Hay rumores de"],
                  "Erupción volcánica": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se produjo",
                                         "Ha empezado",
                                         "Ha comenzado", "Hay rumores de"],
                  "Ola de calor": ["Se ha presenciado", "Se teme", "Se produjo", "Ha empezado", "Ha comenzado",
                                   "Hay rumores de"],
                  "Diluvio": ["Se ha presenciado", "Se teme", "Se produjo", "Ha empezado", "Ha comenzado",
                              "Hay rumores de"],
                  "Sequía": ["Se teme", "Se produjo", "Ha empezado", "Ha comenzado", "Hay rumores de"],
                  "Tormenta": ["Se ha visto", "Se ha presenciado", "Se ha oído", "Se teme", "Se produjo", "Ha empezado",
                               "Ha comenzado", "Hay rumores de"],
                  "Apagón": ["Se ha visto", "Se ha presenciado", "Se teme", "Se produjo", "Ha empezado", "Ha comenzado",
                             "Hay rumores de"]}
        afecta = {"Incendio": ["que afecta a", "dañando a", "abrasando a"],
                  "Terremoto": ["que afecta a", "dañando a", "afectando a", "que ha dañado a"],
                  "Inundación": ["que afecta a", "dañando a", "afectando a", "que ha dañado a", "ahogando a"],
                  "Atentado": ["que afecta a", "dañando a", "afectando a", "que ha dañado a", "en contra de", "contra",
                               "dañando a",
                               "matando a", "atacando a"],
                  "Explosión": ["que afecta a", "dañando a", "afectando a", "que ha dañado a", "en contra de", "contra",
                                "dañando a",
                                "atacando a"],
                  "Brote viríco": ["afectando a", "dañando a"],
                  "Apocalipsis Zombie": ["que afecta a", "dañando a", "afectando a", "que ha dañado a", "en contra de",
                                         "contra", "dañando a",
                                         "atacando a"],
                  "Tiroteo": ["en contra de", "contra", "dañando a", "matando a", "atacando a", "afectando a",
                              "que afecta a"],
                  "Tornado": ["que afecta a", "dañando a", "afectando a", "que ha dañado a", "arrasando con"],
                  "Agresión": ["en contra de", "contra", "dañando a", "matando a", "atacando a"],
                  "Accidente": ["dañando a", "matando a", "atacando a"],
                  "Erupción volcánica": ["dañando a", "matando a", "atacando a", "arasando con", "que afecta a"],
                  "Ola de calor": ["dañando a", "que afecta a", "afectando a", "que ha dañado a"],
                  "Diluvio": ["dañando a", "que afecta a", "afectando a", "que ha dañado a"],
                  "Sequía": ["dañando a", "que afecta a", "afectando a", "que ha dañado a"],
                  "Tormenta": ["dañando a", "afectando a", "que afecta a", "que ha dañado a"],
                  "Apagón": ["afectando a", "que afecta a", "que ha dañado a", "dañando a"]}
        titulo = random.choice(verbos[incidente]) + " un " + incidente.lower() + " " + random.choice(
            adjetivos) + " " + random.choice(
            afecta[incidente]) + " " + str(random.choice(numAfectados)) + " " + random.choice(afectados)
    else:
        # Se elige entre unos títulos que no deberían salir como alertas
        titulos = ["Mi gato está atrapado en el árbol", "Se me ha perdido el perro",
                   "No encuentro el cargador del móvil",
                   "Se me ha cerrado la entrega de AS!!", "No encuentro las gafas", "No encuentro el boli",
                   "No encuentro la barik", "He perdido el metro", "Están repartiendo Coca-Cola's gratis",
                   "Me ha tocado el gordo de la lotería!!"]
        titulo = random.choice(titulos)

    return titulo


def generarGravedad(incidente):
    """
    Generates the importance of the incident given

    Parameters
    ----------
    incidente : str
        Name of the incident reported

    Returns
    -------
    The importance of the incident given (int)
    """

    # Definimos la gravedad mínima y máxima de cada incidente
    gravedades = {"Incendio": [3, 5],
                  "Terremoto": [4, 5],
                  "Inundación": [3, 5],
                  "Atentado": [4, 5],
                  "Explosión": [4, 5],
                  "Brote viríco": [2, 5],
                  "Apocalipsis Zombie": [3, 5],
                  "Tiroteo": [3, 5],
                  "Tornado": [2, 4],
                  "Agresión": [1, 3],
                  "Accidente": [1, 4],
                  "Erupción volcánica": [2, 4],
                  "Ola de calor": [1, 3],
                  "Diluvio": [1, 3],
                  "Sequía": [1, 2],
                  "Tormenta": [1, 2],
                  "Apagón": [1, 4],
                  "Otro": [1, 5]}

    # Elegimos una al azar
    return random.choice(gravedades[incidente])


def generarTags(incidente):
    """
    Generates a list of tags based on the incident given

    Parameters
    ----------
    incidente : str
        Name of the incident reported

    Returns
    -------
    List of tags
    """

    # Definimos las listas de las posibles tags
    posibles_tags = {"Incendio": ["urgente", "ayuda", "personas", "animales", "incendio", "llamarada", "humo",
                                  "chispa", "rastro", "miedo"],
                     "Terremoto": ["urgente", "ayuda", "personas", "animales", "grieta", "terremoto", "escombros",
                                   "temblor", "ruido", "miedo"],
                     "Inundación": ["urgente", "ayuda", "personas", "animales", "agua", "respirar", "inundación",
                                    "río", "lluvia", "miedo"],
                     "Atentado": ["urgente", "ayuda", "personas", "animales", "ruido", "atentado", "disparos",
                                  "heridos", "víctima", "miedo"],
                     "Explosión": ["urgente", "ayuda", "personas", "animales", "grieta", "explosión", "escombros",
                                   "temblor", "ruido", "miedo", "temblor"],
                     "Brote viríco": ["urgente", "ayuda", "personas", "animales", "virus", "COVID", "hospital",
                                      "enfermos", "médicos", "miedo", "síntomas", "brote vírico"],
                     "Apocalipsis Zombie": ["urgente", "ayuda", "personas", "animales", "virus", "zombies",
                                            "enfermedad",
                                            "enfermos", "médicos", "miedo", "síntomas"],
                     "Tiroteo": ["urgente", "ayuda", "personas", "animales", "disparos", "tiroteo", "pistola",
                                 "balas", "heridos", "miedo", "arma"],
                     "Tornado": ["urgente", "ayuda", "personas", "animales", "viento", "ruido", "tornado",
                                 "fuerte", "heridos", "miedo"],
                     "Agresión": ["urgente", "ayuda", "personas", "animales", "gritos", "agresión", "pistola",
                                  "víctima", "heridos", "miedo", "amenazas"],
                     "Accidente": ["urgente", "ayuda", "personas", "animales", "gritos", "accidente", "coche",
                                   "víctima", "heridos", "miedo"],
                     "Erupción volcánica": ["urgente", "ayuda", "personas", "animales", "gritos", "volcán",
                                            "erupción volcánica",
                                            "lava", "fuego", "miedo", "heridos", "evacuar"],
                     "Ola de calor": ["urgente", "ayuda", "personas", "animales", "calor", "ola de calor", "desmayos",
                                      "sofocante", "heridos", "sol", "insoportable", "asfixiante"],
                     "Diluvio": ["urgente", "ayuda", "personas", "animales", "agua", "diluvio", "lluvia",
                                 "frío", "incómodo", "mojado"],
                     "Sequía": ["urgente", "ayuda", "personas", "animales", "sequía", "calor", "seco",
                                "campo", "cultivos", "sed"],
                     "Tormenta": ["urgente", "ayuda", "personas", "animales", "rayos", "truenos", "tormenta",
                                  "electrica", "agua", "lluvia", "miedo"],
                     "Apagón": ["urgente", "ayuda", "personas", "animales", "luz", "apagón", "electricidad",
                                "calefacción", "frío", "calor", "aire acondicionado"],
                     "Otro": ["viral", "selfie", "gato", "perro", "movil", "twitter", "instagram", "whatsapp",
                              "telegram",
                              "universidad", "gratis", "oferta", "lotería"]}

    # Elegimos tags aleatoriamente
    loops = random.randint(3, 7)
    tags = [random.choice(posibles_tags[incidente]) for _ in range(loops)]

    return tags


def generarDistintasCoordenadas(latitud, longitud):
    """
    Variates the given coordinates depending on the place given

    Parameters
    ----------
    latitud : float
    longitud : float

    Returns
    -------
    Geografic coordenates
    """

    operacion = [float.__add__, float.__sub__]
    variacionLatitud = random.uniform(0.0001, 0.0009)
    variacionLongitud = random.uniform(0.0001, 0.0009)

    latitudNueva = random.choice(operacion)(latitud, variacionLatitud)
    longitudNueva = random.choice(operacion)(longitud, variacionLongitud)

    return [latitudNueva, longitudNueva]


def getProvincia(latitud, longitud):
    """
    Get the province of the given coordenates if it's in Spain, otherwise returns "Otra ubicacion" indicating that
    other coordenates are needed

    Parameters
    ----------
    latitud : float
    longitud : float

    Returns
    -------
    Province (str)
    """

    # Definimos la lista de provincias de españa
    provincias_dic = {"Madrid": "Madrid", "Comunidad de Madrid": "Madrid", "Barcelona": "Barcelona", "Valencia":
                       "Valencia", "Sevilla": "Sevilla", "Alicante": "Alicante", "Málaga": "Málaga", "Malaga": "Málaga",
                       "Murcia": "Murcia", "Cádiz": "Cádiz", "Cadiz": "Cádiz", "Vizcaya" : "Bizkaia", "Bizkaia": "Bizkaia",
                       "La Coruña": "A Coruña", "A Coruña": "A Coruña", "Asturias": "Asturias", "Asturies": "Asturias",
                       "Zaragoza": "Zaragoza", "Pontevedra": "Pontevedra", "Granada": "Granada", "Tarragona": "Tarragona",
                       "Córdoba": "Córdoba", "Cordoba": "Córdoba", "Gerona": "Girona", "Girona": "Girona", "Guipúzcoa":
                       "Gipuzkoa", "Guipuzcoa": "Gipuzkoa", "Gipuzkoa": "Gipuzkoa", "Almería": "Almería", "Almeria": "Almería",
                       "Toledo": "Toledo", "Badajoz": "Badajoz", "Jaén": "Jaén", "Jaen": "Jaén", "Navarra": "Navarra",
                       "Comunidad Foral de Navarra": "Navarra", "Nafarroa": "Navarra", "Cantabria": "Cantabria", "Castellón":
                       "Castellón", "Castellon": "Castellón", "Valladolid": "Valladolid", "Ciudad Real": "Ciudad Real",
                       "Huelva": "Huelva", "León": "León", "Leon": "León", "Lérida": "Lérida", "Lerida": "Lérida", "Cáceres":
                       "Cáceres", "Caceres": "Cáceres", "Albacete": "Albacete", "Burgos": "Burgos", "Lugo": "Lugo", "Salamanca":
                       "Salamanca", "Orense": "Ourense", "Ourense": "Ourense", "Álava": "Araba", "Araba": "Araba", "Alava":
                       "Araba", "La Rioja": "La Rioja", "Guadalajara": "Guadalajara", "Huesca": "Huesca", "Cuenca": "Cuenca",
                       "Zamora": "Zamora", "Palencia": "Palencia", "Ávila": "Ávila", "Avila": "Ávila", "Segovia": "Segovia",
                       "Teruel": "Teruel", "Soria": "Soria", "Ceuta": "Ceuta", "Melilla": "Melilla", "Baleares": "Baleares",
                       "Islas Baleares": "Baleares", "Las Palmas": "Las Palmas", "Santa Cruz de Tenerife": "Santa Cruz de Tenerife"}

    geolocator = Nominatim(user_agent="aplicacion_AS")
    # Conseguimos la dirección completa a esas coordenadas
    location = geolocator.reverse(str(latitud) + ", " + str(longitud))
    try:
        direccion = location.address
    except:
        return "Otra ubicacion"
    print(direccion)
    direccion_split = direccion.split(", ")
    provincia = ""

    # Comprobamos si está en España
    if "España" in direccion:

        # Conseguimos la provincia
        for word in direccion_split:
            try:
                provincia = provincias_dic[word]
            except:
                pass
        if provincia != "":
            return provincia
        else:
            return "Otra ubicacion"
    else:
        return "Otra ubicacion"


def subirApi(alerta):
    """
    Execute a "PUT" to the analysis api

    Parameters
    ----------
    alerta : json
        [nombre: str,
        dni: str,
        titulo: str,
        gravedad: int,
        fecha_y_hora: datetime,
        ubicacion: [float, float],
        tags: [str, str, ...],
        provincia: str]
    """
    uri = "http://" + API_IP + ":8000/api/aviso"
    cabeceras = {'Host': 'localhost',
                 'Content-Type': 'application/json'}
    # d = json.dumps(alerta, indent=4, sort_keys=True, default=str)
    # contenido_encoded = urllib.parse.urlencode(alerta)
    # cabeceras['Content-Length'] = str(len(contenido_encoded))
    respuesta = requests.post(uri, json=alerta, headers=cabeceras, allow_redirects=False)
    codigo = respuesta.status_code
    descripsion = respuesta.reason
    if codigo != 200:
        print("Algo ha salido mal")
        print(descripsion, codigo)

# ---------------------------------------
# Main
# ---------------------------------------

if __name__ == '__main__':

    while True:
        # Generamos los datos base a partir de los cuales se van a generar los reportes de alertas
        incidente = generarIncidente()
        latitud, longitud, provincia = generarUbicacion()

        # Número aleatorio de alertas para este incidente
        loops = random.randint(500, 1000)

        for i in range(loops):
            alerta = {}

            # Generamos una persona aleatoria
            alerta["nombre"], alerta["dni"] = generarPersona()

            # Generamos un título aleatorio dependiendo del incidente
            alerta["titulo"] = generarTitulo(incidente)

            # Generamos la gravedad de la alerta según el incidente reportado
            alerta["gravedad"] = generarGravedad(incidente)

            # Guardamos la fecha y hora actual
            alerta["fecha_y_hora"] = datetime.datetime.now().__str__()

            # Generamos una variación de la ubicación creada anteriormente
            alerta["ubicacion"] = generarDistintasCoordenadas(latitud, longitud)

            # Generamos unas tags aleatorias
            alerta["tags"] = generarTags(incidente)

            # Guardamos la provincia
            alerta["provincia"] = provincia

            pprint(alerta)

            subirApi(alerta)

            # Esperamos un tiempo aleatorio entre alertas
            tiempo_entre_alertas = random.randint(1, 5)
            sleep(60 * tiempo_entre_alertas)

        # Se va a generar un incidente cada 60 * tiempo_entre_incidentes
        tiempo_entre_incidentes = random.randint(1, 20)
        sleep(60 * tiempo_entre_incidentes)